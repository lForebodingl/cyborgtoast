# CyborgToast [InDev]

[![mit](https://img.shields.io/badge/Licensed%20under-MIT-red.svg?style=flat-square)](./LICENSE) [![made-with-python](https://img.shields.io/badge/Made%20with-Python%203.7-ffde57.svg?longCache=true&style=flat-square&colorB=ffdf68&logo=python&logoColor=88889e)](https://www.python.org/) [![made-with-discord.py](https://img.shields.io/badge/Using-discord.py-ffde57.svg?longCache=true&style=flat-square&colorB=4584b6&logo=discord&logoColor=7289DA)](https://github.com/Rapptz/discord.py) [![Built with libneko](https://img.shields.io/badge/built%20with-libneko-ff69b4.svg?longCache=true&style=flat-square)](https://gitlab.com/koyagami/libneko)
[![support](https://img.shields.io/discord/461271067923841025.svg?style=flat-square&label=Support)](https://discord.gg/T5KbFBy)


## This page is under a revamp. Check back later ;)

  
  **Credits**
  
  I've developed almost all the features. The Hypixel API wrapper that I use for the Hypixel commands is so kindly provided by Snuggle (visit him [here](https://github.com/Snuggle)) and I also use [libneko](https://gitlab.com/neko404notfound/libneko) which is mainly made by the Espy, God himself :P (visit him and give him some love [here](https://github.com/neko404notfound/)).
  
  
  Check the wiki for the complete list of features that this bot has to offer (still in the works)! Feel free to make any suggestions by DMing me @Tmpod#1004 or using the bot's feedback command ;) 
