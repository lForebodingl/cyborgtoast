#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
from discord.ext.commands import Cog
from libneko.extras.superuser import SuperuserCog

from toaster.utils import with_category


@with_category()
class SuperUser(SuperuserCog):
    ...

def setup(bot):
    bot.add_cog(SuperUser())
    