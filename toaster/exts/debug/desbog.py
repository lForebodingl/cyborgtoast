#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
import discord
from discord.ext.commands import Cog
from libneko import commands
import asyncio
import inspect
import aiohttp
from libneko import embeds
from textwrap import dedent
from libneko.pag.factory import embedfactory
import shutil
import aiofiles
from os.path import realpath

from toaster import __url__
from toaster.utils import strings, misc


@misc.with_category()
class Debug:
    def __init__(self, bot):
        self.bot = bot

    # @commands.is_owner()
    # @commands.group(
    #     name="debug",
    #     aliases=('dg',),
    #     hidden=True,
    #     invoke_without_subcommand=True)
    # async def commands(self, ctx):
    #     """Debug/Bot Dev commands"""
    #     if ctx.invoked_subcommand is None:
    #         await ctx.message.add_reaction("\N{BLACK QUESTION MARK ORNAMENT}")
    #         await asyncio.sleep(5)
    #         member = utils.find(lambda m: m == self.bot.user, ctx.guild.members)
    #         await ctx.message.remove_reaction("\N{BLACK QUESTION MARK ORNAMENT}", member)

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.category)
    @commands.has_permissions(manage_messages=True)
    @commands.command(name="emojitest", brief="Sends all server emojis", hidden=True)
    async def emoji_test(self, ctx):
        await ctx.send(strings.pretty_list(ctx.guild.emojis, sep=" "))

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.channel)
    @commands.command(
        name="source", aliases=("src",), brief="Get's the code behind a command"
    )
    async def get_cmd_source(self, ctx, *, command: str = None):
        """Sends a paginated embed nav session with the source of the given command"""
        if command is None:
            return await ctx.invoke(self.bot.get_command("git repo"))

        cmd = self.bot.get_command(command.replace(".", " "))
        if not cmd:
            raise UserWarning(f"{self.bot.crossmark} That's not a valid command")

        source, firstline = inspect.getsourcelines(cmd.callback.__code__)

        raw_branches = (await misc.run_in_shell("git branch")).split("\n")
        for b in raw_branches:
            if b.startswith("*"):
                branch = b[2:]

        location = realpath(cmd.callback.__code__.co_filename).replace("/bakery", "")
        url = f"<{__url__[:-4]}/blob/{branch}/{location}#L{firstline}-L{firstline + len(source) - 1}>"
        nav = embedfactory.EmbedNavigatorFactory(
            prefix=f"Source for **{cmd.qualified_name}**\n[**Repo Link**]({url})```py",
            suffix="```",
        )
        nav.add_block("".join(source))
        nav.build(ctx).start()

    @commands.is_owner()
    @commands.command(
        name="botavatar",
        aliases=("bav",),
        hidden=True,
        brief="Changes the avatar to the given URL.",
    )
    async def set_bot_avatar(self, ctx, *, url: str):
        async with aiohttp.ClientSession() as session:
            async with session.get(url) as r, ctx.typing():
                await ctx.bot.user.edit(avatar=await r.read())
        await ctx.send(
            embed=embeds.Embed(description="Successfully changed my avatar!")
        )

    @commands.is_owner()
    @commands.command(
        name="reboot", aliases=("rb", "kys"), brief="Reboots the bot", hidden=True
    )
    async def reboot_bot(self, ctx, options: str = None):
        no_pull, install_dep = (0, 0)
        opt_actions = ("--no-pull", "-np", "--install-dep", "-idp")
        if options:
            options = set(options.split())
            # install_dep, no_update = _parse_args(options, opt_actions)
            for opt in options:
                if opt not in opt_actions:
                    return await ctx.send(
                        embed=embeds.Embed(
                            description=f"{self.bot.crossmark} You provided an invalid option!",
                            colour=discord.Colour.red(),
                            timestamp=None,
                        )
                    )
                if opt in opt_actions[:2]:
                    no_pull = 1
                elif opt in opt_actions[2:4]:
                    install_dep = 1

        if no_pull and install_dep:
            return await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.crossmark} You provided incompatible options!",
                    colour=discord.Colour.red(),
                    timestamp=None,
                )
            )
        self.bot.logger.info(f"Rebooting by order from `{ctx.author}`")
        msg = await ctx.send(
            embed=embeds.Embed(
                description="<a:loading:466977962374463490> CyborgToast is restarting...",
                colour=discord.Colour.gold(),
                timestamp=None,
            )
        )
        shutil.rmtree("/breadvan/REBOOT", ignore_errors=True)
        async with aiofiles.open("/breadvan/REBOOT", "w") as fp:
            await fp.write(f"{no_pull}\n{install_dep}\n{ctx.channel.id}\n{msg.id}")
        await self.bot.logout()

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.guild)
    @commands.has_permissions(manage_emojis=True)
    @commands.command(
        name="addemoji", aliases=["ae"], brief="Adds an emoji to the server"
    )
    async def add_emoji(self, ctx, name: str, url: str):
        """Adds an emoji to the server with the given name and image (from url)"""
        try:
            async with aiohttp.request("GET", url) as r:
                img = await r.read()
            await ctx.guild.create_custom_emoji(name=name, image=img)
            await ctx.send(
                embed=discord.Embed(
                    description=f"{self.bot.checkmark} Emoji successfuly added!",
                    colour=discord.Colour.green(),
                )
            )
        except discord.errors.HTTPException:
            await ctx.send(
                embed=discord.Embed(
                    description=f"{self.bot.crossmark} Emoji image too big! It must be at max 256kb!",
                    colour=discord.Colour.red(),
                )
            )

    @commands.command(
        name="getemoji", brief="Sends the emoji string for bots", hidden=True
    )
    async def get_emoji_string(self, ctx, emoji: discord.Emoji):
        """Gets you the emoji in the `<:name:id>` format (`<a:name:id>` if it's animated!`"""
        await ctx.send(f"```<:{emoji.name}:{emoji.id}>```")

    @commands.cooldown(rate=1, per=30, type=commands.BucketType.channel)
    @commands.command(
        name="confirmscreen",
        aliases=("cfs",),
        brief="Makes a demonstrations of a confirmation screen",
        hidden=True,
    )
    async def confirm_screen_demo(
        self,
        ctx,
        text: str = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    ):
        """Tests the confirmation screen feature"""
        emb = embeds.Embed(description=text, colour=discord.Colour.gold())
        emb.set_footer(text="This will timeout in 10s")

        msg = await ctx.send(embed=emb)
        result = await misc.confirm_opts(self.bot, msg, ctx.author)
        await msg.edit(
            embed=embeds.Embed(
                description=f"{text}\n**Result:** `{result}`",
                colour=discord.Colour.green() if result else discord.Colour.red(),
            )
        )

    @commands.is_owner()
    @commands.command(aliases=["cp"], hidden=True)
    async def clearpins(self, ctx):
        if ctx.message.channel.id == 437_626_535_805_976_586:
            em = discord.Embed(
                description=f":question: Are you sure?", color=discord.Colour.gold()
            )
            em.set_footer(text="This action will stop in 5 seconds")
            confirm = await ctx.send(embed=em)
            yup = await confirm.add_reaction("\N{WHITE HEAVY CHECK MARK}")
            try:
                reaction, user = await self.bot.wait_for(
                    "reaction_add",
                    timeout=5,
                    check=lambda r, u: u == ctx.author
                    and r.emoji == ("\N{WHITE HEAVY CHECK MARK}"),
                )
            except asyncio.TimeoutError:
                return
            finally:
                await confirm.delete()
            if reaction.emoji == "\N{WHITE HEAVY CHECK MARK}":
                print("\nzxcv ok")
                msg = await ctx.send(
                    embed=discord.Embed(
                        description="Clearing all pins in 5 seconds!",
                        color=discord.Colour.dark_red(),
                    )
                )
                await asyncio.sleep(5)
                await asyncio.gather(
                    *[p.delete() for p in (await ctx.message.channel.pins())]
                )
                await msg.edit(
                    embed=discord.Embed(
                        description="**All pins cleared**!",
                        color=discord.Colour.dark_red(),
                    )
                )
        else:
            await ctx.send(
                embed=discord.Embed(
                    description=f"<:stop:469543643124989962> You cannot execute that command here! {ctx.author.mention}",
                    color=discord.Colour.red(),
                )
            )

    @commands.command(aliases=["grid"], hidden=True)
    async def getroleid(self, ctx, *, role: discord.Role):
        await ctx.send(f"```{role.id}```")

    @commands.is_owner()
    @commands.command(aliases=["fgs"], hidden=True)
    async def forceguildsetup(self, ctx):
        for g in self.bot.guilds:
            if await self.bot.mongodb.guilds.find_one({"_id": g.id}):
                continue
            await self.bot.mongodb.guilds.insert_one({"_id": g.id})
        # prefix_key = f"guilds:{ctx.guild.id}:prefixes"
        # general_key = f"guilds:{ctx.guild.id}"
        # try:
        #     for p in self.bot.default_prefixes:
        #         await self.bot.redis.sadd(prefix_key, p)
        #     await self.bot.redis.hset(general_key, "name", ctx.guild.name)
        #     await self.bot.redis.hset(general_key, "max_parties", max)
        #     await self.bot.redis.hset(general_key, "bots_role", bots)
        #     await self.bot.redis.hset(general_key, "party_ads", ads)
        #     await ctx.send(
        #         embed=embeds.Embed(
        #             description=f"{self.bot.checkmark} Successfuly forced setup!",
        #             colour=discord.Colour.green(),
        #             timestamp=None,
        #         )
        #     )
        # except Exception as ex:
        #     self.bot.logger.error(
        #         f"Could not froce guild setup with error: %s", ex, exc_info=True
        #     )
        #     # await cat.delete()
        #     raise UserWarning("Could not force guild setup. Check logs for more info.")


def setup(bot):
    """Defining this extension"""
    bot.add_cog(Debug(bot))
