#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
import discord
from discord.ext.commands import Cog
from libneko import commands
import json
import os
import random
import asyncio
import async_timeout
import time
import collections
from libneko import embeds
from toaster.utils import misc, strings


@misc.with_category()
class Fun:
    def __init__(self, bot):
        self.bot = bot
        self.respects = {}

    @commands.command(hidden=True, aliases=["ily"])
    async def iloveyou(self, ctx):
        await ctx.send(f"{ctx.author.mention} I love myself too! :heart::heart::heart:")

    @commands.command(aliases=("d", "dice"), brief="Rolls a die.")
    async def die(self, ctx, faces: int = 6):
        """
        Rolls die and sends the result!
        You can provide a number of faces or ommit it to roll a 6-faced die.
        """
        if not 1 <= faces <= 10000:
            raise UserWarning(f"{self.bot.crossmark} You've entered an invalid number of faces. It must be between 1 and 10000!")
            
        msg = await ctx.send(
            embed=embeds.Embed(description="Rolling the dice...", color=misc.rand_hex())
        )
        await asyncio.sleep(2)
        await msg.edit(
            embed=embeds.Embed(
                description=f"You rolled **{random.randint(1, faces)}**!",
                color=misc.rand_hex(),
            )
        )

    @commands.command(aliases=["c"], brief="Flips a coin.")
    async def coin(self, ctx):
        """Flips a coin a sends the resutl!"""
        msg = await ctx.send(
            embed=embeds.Embed(
                description="Flipping the coin...", colour=misc.rand_hex()
            )
        )
        await asyncio.sleep(2)
        await msg.edit(
            embed=embeds.Embed(
                description=f"You flipped **{random.choice(('Tales', 'Heads'))}**!",
                colour=misc.rand_hex(),
            )
        )

    @commands.command(
        aliases=["m8b"], brief="The Magic8Ball has the answers for everything..."
    )
    async def magic8ball(self, ctx, *, smth=None):
        """Ask something to the Magic8Ball and start predicting your future. It works. Believe me. I'm serious."""
        answers = [
            "It is certain",
            "It is decidedly so",
            "Without a doubt",
            "Yes definitely",
            "You may rely on it",
            "You can count on it",
            "As I see it, yes",
            "Most likely",
            "Outlook good",
            "Yes",
            "Signs point to yes",
            "Absolutely",
            "Reply hazy try again",
            "Ask again later",
            "Better not tell you now",
            "Cannot predict now",
            "Concentrate and ask again",
            "Don't count on it",
            "My reply is no",
            "My sources say no",
            "Outlook not so good",
            "Very doubtful",
            "Chances aren't good",
        ]
        if smth == None:
            await ctx.send(
                embed=discord.Embed(
                    description=f":8ball: You haven't asked anything! {ctx.author.mention}",
                    color=misc.rand_hex(),
                )
            )
        else:
            await ctx.trigger_typing()
            await asyncio.sleep(1)
            await ctx.send(
                embed=discord.Embed(
                    description=":8ball: " + random.choice(answers),
                    color=misc.rand_hex(),
                )
            )

    @commands.command(
        aliases=["bg"],
        brief="Makes your text 🇧 🇮 🇬",
        disabled=True,
    )
    async def bigtext(self, ctx, *, text: str):
        s = ""
        for char in text:
            if char.isalpha():
                s += f":regional_indicator_{char.lower()}: "
            elif char.isspace():
                s += "   "
            elif char.isdigit():
                s += f"{char}\N{COMBINING ENCLOSING KEYCAP}"
            else:
                s += f"**{char}**"

        await ctx.send(s)

    @commands.cooldown(rate=1, per=500, type=commands.BucketType.guild)
    @commands.is_owner()
    @commands.command(aliases=["cd"], brief="3, 2, 1, Time's Up!", hidden=True)
    async def countdown(self, ctx, timer: int = None):
        """Created a timer with the given time."""
        if timer is None:
            await ctx.send(
                embed=discord.Embed(
                    description=":watch: I want a time!", color=discord.Colour.red()
                )
            )
        else:
            if timer <= 0:
                await ctx.send(
                    embed=discord.Embed(
                        description=":octagonal_sign: That's not a valid time!",
                        color=discord.Colour.red(),
                    )
                )
            elif timer > 1000:
                await ctx.send(
                    embed=discord.Embed(
                        description=":octagonal_sign: That time is too big! It must be between 1 and 1001",
                        color=discord.Colour.red(),
                    )
                )
            else:
                msg = await ctx.send(
                    embed=discord.Embed(
                        description="**Starting countdown!**",
                        color=discord.Colour.orange(),
                    )
                )
                loop = ctx.bot.loop
                await asyncio.sleep(0.5)
                stop = "\N{BLACK SQUARE FOR STOP}"
                await msg.add_reaction(stop)
                try:
                    reaction, user = await self.bot.wait_for(
                        "reaction_add",
                        timeout=timer + 2,
                        check=lambda r, u: u == ctx.author and r.emoji in (stop),
                    )
                except asyncio.TimeoutError:
                    pass
                finally:
                    msg.delete()
                for t in range(timer, 0, -1):
                    mins, secs = divmod(t, 60)
                    loop.create_task(
                        msg.edit(
                            embed=discord.Embed(
                                description=f"**{mins:,}:{secs:02}**",
                                color=discord.Colour.orange(),
                            )
                        )
                    )
                    await asyncio.sleep(1)
                await msg.edit(
                    embed=discord.Embed(
                        description=":watch::exclamation: Time's up!",
                        color=discord.Colour.red(),
                    )
                )
                ping = await ctx.send(ctx.author.mention)
                await ping.delete()

    @commands.command(
        aliases=("rr",), brief="Never gonna give you up, never gonna let you down..."
    )
    async def rickroll(self, ctx):
        """Sends a RickRoll gif, because why not?"""
        em = discord.Embed(color=misc.rand_hex())
        em.set_image(url="https://media.giphy.com/media/kFgzrTt798d2w/giphy.gif")
        await ctx.send(embed=em)

    @commands.command(
        name="love",
        aliases=("<:bloblove:470356905823567882>",),
        brief="For agg ;D",
        hidden=True,
    )
    async def blob_love(self, ctx):
        await ctx.send("<a:abloblove:479206822788464640>")

    @commands.command(aliases=("bar", "baz"), brief="Devs are awkward tbh...")
    async def foo(self, ctx):
        """Sends a random dev word"""
        opt = ["foo", "bar", "baz"]
        opt.remove(ctx.invoked_with)
        await ctx.send(f"```{random.choice(opt)}```")

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.channel)
    @commands.command(name="respect", aliases=["f"], brief="Pay your respects.")
    async def pay_respect(self, ctx, *, torespect: str = None):
        """
        Sends a "pay respect" message that people can react to to pay their respects.
        It will expire either in 5 hours or if someone changes the subject within the time limit.
        """
        prepositions = ("for ", "because ", "to ")
        raw_paidrespect = collections.OrderedDict()
        raw_paidrespect[str(ctx.author)] = None
        end_time = time.monotonic() + 900

        await ctx.message.delete()

        em = embeds.Embed(colour=misc.rand_hex())

        if torespect:
            if torespect.startswith(prepositions):
                ending = f" {torespect}"
            else:
                ending = f" for {torespect}"
        else:
            ending = "\N{ZERO WIDTH SPACE}"

        em.description = f"{ctx.author} paid his/her respects{ending}."

        f_msg = await ctx.send(embed=em)
        f_btn = "\N{REGIONAL INDICATOR SYMBOL LETTER F}"
        await f_msg.add_reaction(f_btn)

        key = f"{ctx.channel.id}@{ctx.guild.id}"
        if key in self.respects:
            old_task = self.respects[key][1]
            old_f_msg = await ctx.channel.get_message(int(self.respects[key][0]))
            old_task.cancel()
            await old_f_msg.clear_reactions()
            old_em = old_f_msg.embeds[0]
            old_em.set_footer(text=f"The subject was changed by {ctx.author}. Shame...")
            await old_f_msg.edit(embed=old_em)

        async def respect_task(self):
            while end_time - time.monotonic():

                def check(reaction, user):
                    rcheck = reaction.emoji == f_btn
                    mcheck = reaction.message.id == f_msg.id
                    ucheck = not user.bot and user != ctx.author
                    return rcheck and mcheck and ucheck

                try:
                    reaction, user = await self.bot.wait_for(
                        "reaction_add", timeout=18000, check=check
                    )
                    rcheck = reaction.emoji == f_btn
                    mcheck = reaction.message.id == f_msg.id
                    ucheck = not user.bot and user != ctx.author
                    if rcheck and mcheck and ucheck:
                        raw_paidrespect[str(user)] = None
                        paidrespect = strings.pretty_list(
                            [*raw_paidrespect.keys()], sep=", ", p_end=" and "
                        )
                        em.description = (
                            f"{paidrespect} have paid their respects{ending}."
                        )
                        await f_msg.edit(embed=em)
                except asyncio.TimeoutError:
                    break

            em.set_footer(text="Expired")
            await f_msg.edit(embed=em)
            await f_msg.clear_reactions()

        respect = self.bot.loop.create_task(respect_task(self))
        self.respects.update(
            {f"{ctx.channel.id}@{ctx.guild.id}": (f"{f_msg.id}", respect)}
        )


def setup(bot):
    """Defining this extension"""
    bot.add_cog(Fun(bot))
