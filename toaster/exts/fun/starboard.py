#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
A starboard feature
"""

from asyncio import create_task, BoundedSemaphore, sleep
import discord
from discord.ext.commands import Cog
from discord.ext.commands import Cog
from libneko import commands, checks
from asyncio import TimeoutError as AIOTimeoutError
from libneko import embeds
from typing import Optional, Union
import re

from toaster.utils import (
    MessageConverter,
    ColourConverter,
    CommandConverter,
    rand_hex,
    embed_to_str,
    with_category,
)

STAR_URL = "https://image.ibb.co/m222zf/small-star.png"

FOOTER_PATTERN = re.compile(r"This is star post #(\d+)")

STAR_EMOJI = "\N{WHITE MEDIUM STAR}"

SUPER_STAR = "\N{GLOWING STAR}"

DEFAULT_VOTE_COUNT = 5


@with_category()
class Starboard:
    """
    The cog holding the starboard commands and events
    """

    def __init__(self, bot):
        self.bot = bot
        self.cooldowns = {
            # Should have this form:
            # guild.id: cooldown_semaphore,
            # ...
        }

    async def _start_cooldown(self, code, duration: float = 20.0):
        sem = BoundedSemaphore()
        self.cooldowns[code] = sem
        await sem.acquire()
        await sleep(duration)
        sem.release()

    @staticmethod
    async def _get_last_star_number(bot, channel: discord.TextChannel):
        """
        Gets last star number. It searches for 300 messages, 
        if it doesn't find anything, the handler will reset the counter. 
        """
        async for msg in channel.history(limit=300):
            if msg.embeds and msg.author == bot.user:
                footer_txt = msg.embeds[0].footer.text
                if isinstance(footer_txt, discord.embeds._EmptyEmbed):
                    continue
                num = FOOTER_PATTERN.findall(footer_txt)
                if num:
                    return int(num[0])

    async def on_raw_reaction_add(self, payload):
        """
        Handling when a potential star reaction is added. For a message to be starposted 
        it needs to have 5 upvotes in the form of star reactions. This has a **20s** cooldown.
        The bot creator has the power of the super star wich instantly stars a post, 
        for testing purposes.
        This feature is not available on DMs obviously.
        """
        user = self.bot.get_user(payload.user_id) # Use the internal cache

        if user is None or user.bot:
            return

        guild = self.bot.get_guild(payload.guild_id)
        ch = guild.get_channel(payload.channel_id)
        msg = await ch.get_message(payload.message_id)
        if (
            guild is None  # No DMs
            or self.cooldowns.get(
                guild_id, BoundedSemaphore()
            ).locked()  # Checking the cooldown
            or payload.emoji.name
            not in (STAR_EMOJI, SUPER_STAR)  # This needs to be a valid emoji
        ):
            return

        vote_count = (
            await self.bot.redis.hget(f"guilds:{guild_id}", "vote_count")
            or DEFAULT_VOTE_COUNT
        )

        ch = guild.get_channel(payload.channel_id)
        msg = await ch.get_message(payload.message_id)

        if reaction.count >= int(vote_count) or (
            payload.emoji.name == SUPER_STAR
            and (
                ch.permissions_for(user).manage_messages
                or user == self.bot.creator
            )
        ):  # Testing purposes
            starboard = (
                await self.bot.mongodb.guilds.find_one(
                    {"_id": guild.id}, {"starboard": 1}
                )
            ).get("starboard")

            if not starboard:
                return await msg.add_reaction(self.bot.crossmark)

            star_ch = guild.get_channel(starboard["channel"])

            if (
                msg.webhook_id  # Might implement this later
                or await self.bot.redis.sismember("startposts", message_id)  # Checking if the post is already starred
                or ch == star_ch  # Checking if the post is a star post
            ):
                return await msg.add_reaction(self.bot.maybemark)

            if msg.embeds:
                footer_txt = msg.embeds[0].footer.text
                if isinstance(footer_txt, discord.embeds._EmptyEmbed):
                    pass
                elif FOOTER_PATTERN.findall(footer_txt):
                    return await msg.add_reaction(self.bot.crossmark)

                desc = embed_to_str(msg.embeds[0])
            else:
                desc = msg.content

            emb = embeds.Embed(
                description=(
                    f"\N{ZERO WIDTH SPACE}\n{desc}\n\n"
                    "[***Jump to original***]({msg.jump_url})"
                ),
                colour=discord.Colour(starboard["colour"]),
                timestamp=msg.created_at,
            )
            emb.set_author(name=msg.author.display_name, icon_url=msg.author.avatar_url)

            if msg.attachments:  # Handling attachments
                if msg.attachments[0].width:  # Seeing if it's an image
                    emb.set_image(url=msg.attachments[0].proxy_url)
                else:
                    emb.add_field(name="Attachment", value=msg.attachments[0].proxy_url)

            if not starboard["done_first"]:
                post_number = 0
                await self.bot.mongodb.guilds.update_one(
                    {"_id": guild.id}, {"$set": {"starboard.done_first": True}}
                )
            else:
                post_number = await self._get_last_star_number(self.bot, star_ch)
                if not post_number:
                    post_number = 0

            emb.set_footer(
                text=f"This is star post #{int(post_number) + 1}", icon_url=STAR_URL
            )

            await star_ch.send(embed=emb)
            await msg.add_reaction(self.bot.checkmark)

            await self.bot.redis.sadd("starposts", message_id)

            create_task(self._start_cooldown(msg.guild.id))  # Starting the cooldown

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.channel)
    @commands.bot_has_permissions(read_message_history=True)
    @commands.group(
        name="starboard",
        aliases=("star", "sb"),
        brief="A neat starboard implementation",
        case_insensitive=True,
    )
    async def starboard_group(self, ctx):
        """
        This allows you to easily setup a starboard on your server.
        Use the `settings` subgroup to configure the starboard at your will. 

        **__How to use the starboard?__**
            This feature works with a upvote system, meaning that for a message to be starred it 
            needs to be upvoted X times. That is done by reacting with :star:
            The number of needed upvotes is customizable with the `votecount` (under `settings`).

        **__Special Permissions__**
            Members with `Manage Messages` can use :star2: and bypass the vote count.

        *__Note:__ If an error occurs the bot will add a cross reaction and if the feature is on cooldown
        a yellow mark will be added*
        """
        # await self.star_post.callback(self, ctx, post)
        pass

    # @commands.cooldown(rate=1, per=10, type=commands.BucketType.channel)
    # @commands.has_permissions(manage_messages=True)
    # @commands.bot_has_permissions(read_message_history=True)
    # @starboard_group.command(name="post", aliases=("p",), brief="Makes a star post")
    # async def star_post(self, ctx, post: MessageConverter):
    #     """
    #     Stars the specified message and posts it on the registered starboard channel with the registered colour.
    #     You can either provide a message ID (it will search for it in the channel the command is used on) or a
    #     jump url (click on the three dots next to the 'Add Reaction' button and click on 'Copy Link')
    #     """
    #     starboard = (await self.bot.mongodb.guilds.find_one({"_id": ctx.guild.id})).get("starboard")
    #     if not starboard:
    #         return await ctx.send(
    #             embed=embeds.Embed(
    #                 description=(
    #                     f"{self.bot.crossmark} The starboard hasn't been setup yet!"
    #                     f" Use `{ctx.prefix}star ch <channel>` to start it."
    #                 ),
    #                 colour=discord.Colour.red(),
    #                 timestamp=None,
    #             )
    #         )

    #     if post.webhook_id:
    #         return await ctx.send(
    #             embed=embeds.Embed(
    #                 description=(
    #                     f"{self.bot.crossmark} Cannot star post webhook messages yet!"
    #                     "*(being worked on)*"
    #                 ),
    #                 colour=discord.Colour.red(),
    #                 timestamp=None,
    #             )
    #         )

    #     if post.embeds:
    #         desc = embed_to_str(post.embeds[0])
    #     else:
    #         desc = post.content

    #     emb = embeds.Embed(
    #         description=f"\N{ZERO WIDTH SPACE}\n{desc}\n\n[*Jump to original*]({post.jump_url})",
    #         colour=discord.Colour(starboard["colour"]),
    #         timestamp=post.created_at,
    #     )
    #     emb.set_author(name=post.author.display_name, icon_url=post.author.avatar_url)

    #     star_ch = ctx.guild.get_channel(starboard["channel"])

    #     if not starboard["done_first"]:
    #         post_number = 0
    #         await self.bot.mongodb.starboard.update_one(
    #             {"_id": ctx.guild.id}, {"$set": {"done_first": True}}
    #         )
    #     else:
    #         post_number = await self._get_last_star_number(self.bot, star_ch)
    #         if not post_number:
    #             notice = await ctx.send(
    #                 embed=embeds.Embed(
    #                     description=(
    #                         f"<:maybe:503143206704250880> I cannot get the star post number,"
    #                         " due to the starboard having too many recent 'non-star' posts"
    #                         " (you should keep it clean of normal messages)!"
    #                         "\n**Please specify a number to be used instead.**"
    #                     ),
    #                     colour=discord.Colour.gold(),
    #                     timestamp=None,
    #                 )
    #             )
    #             try:
    #                 msg = await self.bot.wait_for(
    #                     "message", check=lambda m: m.author == ctx.author, timeout=30
    #                 )
    #                 try:
    #                     post_number = int(msg.content) - 1
    #                 except ValueError:
    #                     return await notice.edit(
    #                         embed=embeds.Embed(
    #                             description=f"{self.bot.crossmark} That's not a number... Try again later!",
    #                             colour=discord.Colour.red(),
    #                             timestamp=None,
    #                         )
    #                     )
    #             except AIOTimeoutError:
    #                 return await notice.edit(
    #                     embed=embeds.Embed(
    #                         description=f"{self.bot.crossmark} You took too long... Try again later!",
    #                         colour=discord.Colour.red(),
    #                         timestamp=None,
    #                     )
    #                 )

    #     emb.set_footer(
    #         text=f"This is star post #{int(post_number) + 1}", icon_url=STAR_URL
    #     )

    #     star_post = await star_ch.send(embed=emb)
    #     await ctx.send(
    #         embed=embeds.Embed(
    #             description=f"{self.bot.checkmark} Successfully starred [this post]({post.jump_url})!"
    #             f"\nCheck it out [here]({star_post.jump_url})"
    #         )
    #     )

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.channel)
    @commands.has_permissions(manage_messages=True)
    @commands.bot_has_permissions(read_message_history=True)
    @starboard_group.command(
        name="remove", aliases=("rm", "unstar"), brief="Removes a star post"
    )
    async def star_remove(self, ctx, post: Union[MessageConverter, int]):
        """
        Removes the star post with the given post number or as the given message.
        To pass a message you can either give the message ID (requires developer mode) or the message jump link.
        """
        error_emb = embeds.Embed(
            description=f"{self.bot.crossmark} That's not a star post! *(at least not by me)*",
            colour=discord.Colour.red(),
            timestamp=None,
        )

        await ctx.trigger_typing()  # This make take a second or so

        if isinstance(post, int):
            starboard = (
                await self.bot.mongodb.guilds.find_one({"_id": ctx.guild.id})
            ).get("starboard")
            star_ch = ctx.guild.get_channel(starboard["channel"])
            async for msg in star_ch.history(limit=300):
                if msg.embeds and msg.author == self.bot.user:
                    footer_txt = msg.embeds[0].footer.text
                    if isinstance(footer_txt, discord.embeds._EmptyEmbed):
                        continue
                    num = FOOTER_PATTERN.findall(footer_txt)
                    if not num:
                        continue
                    if int(num[0]) == post:
                        post = msg
            if isinstance(post, int):  # Post wasn't found
                return await ctx.send(
                    embed=embeds.Embed(
                        description=f"{self.bot.crossmark} Post **#{post}** is too old to be deleted by me!",
                        colour=discord.Colour.red(),
                        timestamp=None,
                    )
                )

        if post.author != self.bot.user:
            return await ctx.send(embed=error_emb)

        emb = post.embeds[0]
        try:
            if not isinstance(emb.footer.text, discord.embeds._EmptyEmbed):
                number = FOOTER_PATTERN.findall(emb.footer.text)
                if not number:
                    return await ctx.send(embed=error_emb)
                await post.delete()
                await ctx.send(
                    embed=embeds.Embed(
                        description=f"{self.bot.checkmark} Successfully removed star post **#{number[0]}**",
                        colour=discord.Colour.green(),
                        timestamp=None,
                    )
                )
            else:
                await ctx.send(embed=error_emb)
        except AttributeError:
            await ctx.send(embed=error_emb)

    @starboard_group.group(
        name="settings",
        aliases=("st",),
        brief="Control how the starboard behaves",
        case_insensitive=True,
        invoke_without_command=True,
    )
    async def star_settings(self, ctx):
        """
        Fine tune the starboard settings at you will.
        You can change the starboard channel and the colour of the star posts.
        """
        await ctx.invoke(self.star_overview)

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.channel)
    @star_settings.command(
        name="overview",
        aliases=("ov",),
        brief="Gives an overview of the current settings",
    )
    async def star_overview(self, ctx):
        """
        This will you a general view of the current registered settings for the starboard.
        """
        colour = rand_hex()

        starboard = (
            await self.bot.mongodb.guilds.find_one(
                {"_id": ctx.guild.id}, {"starboard": 1}
            )
        ).get("starboard")
        if starboard:
            colour = discord.Colour(starboard["colour"])
            ch = discord.utils.get(ctx.guild.channels, id=starboard["channel"])
            footer = f"Explore the settings with `{ctx.prefix}help star st`"
        else:
            footer = (
                "The starboard hasn't been setup yet. "
                f"Use the channel command to initialize it (`{ctx.prefix}star st ch <channel>`)"
            )

        em = embeds.Embed(
            title="Starboard overview",
            description="Here're the current settings for the starboard!",
            colour=colour,
        )
        if starboard:
            em.add_field(name=":card_index: Channel", value=ch.mention, inline=False)
            em.add_field(
                name=":art: Colour", value=colour or f"*`Default`* Gold", inline=False
            )
            em.add_field(
                name=":1234: Needed upvotes",
                value=f"**`{starboard['vote_count']}`**",
                inline=False,
            )
        em.set_footer(text=footer)

        await ctx.send(embed=em)

    @commands.has_permissions(manage_channels=True)
    @commands.cooldown(rate=1, per=10, type=commands.BucketType.guild)
    @star_settings.command(
        name="votecount", aliases=("vc",), brief="Change the no. of upvotes needed"
    )
    async def star_vote_count(self, ctx, count: int = 5):
        """Change the registered channel for the starboard, by passing a name, ID or mention."""
        if count < 2:
            raise UserWarning(f"`{count}` is too low. It must be 2 at least.")

        if count > 25:
            raise UserWarning(f"`{count}` is too high. It must be 25 at max.")

        starboard = (
            await self.bot.mongodb.guilds.find_one(
                {"_id": ctx.guild.id}, {"starboard": 1}
            )
        ).get("starboard")
        if starboard is None:
            raise UserWarning(
                "The starboard hasn't been setup yet."
                f"Use the channel command to initialize it (`{ctx.prefix}star st ch <channel>`)"
            )

        if starboard["vote_count"] == count:
            raise UserWarning(
                f"{self.bot.maybemark} That is the registered value already :wink:",
                "invis",
            )

        await self.bot.redis.hset(f"guilds:{ctx.guild.id}", "vote_count", count)
        await self.bot.mongodb.guilds.update_one(
            {"_id": ctx.guild.id}, {"$set": {"starboard.vote_count": count}}
        )

        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.checkmark} Changed the vote count from **`{starboard['vote_count']}`** to **`{count}`**",
                timestamp=None,
            )
        )

    @commands.has_permissions(manage_channels=True)
    @commands.cooldown(rate=1, per=10, type=commands.BucketType.guild)
    @star_settings.command(
        name="channel", aliases=("ch",), brief="Change the starboard channel"
    )
    async def star_channel(self, ctx, channel: discord.TextChannel = None):
        """Change the registered channel for the starboard, by passing a name, ID or mention."""
        if channel is None:
            channel = ctx.channel

        starboard = (
            await self.bot.mongodb.guilds.find_one(
                {"_id": ctx.guild.id}, {"starboard": 1}
            )
        ).get("starboard")
        if starboard is None:
            await self.bot.mongodb.guilds.update_one(
                {"_id": ctx.guild.id},
                {
                    "$set": {
                        "starboard": {
                            "channel": channel.id,
                            "vote_count": DEFAULT_VOTE_COUNT,
                            "colour": 15_844_367,
                            "done_first": False,
                        }
                    }
                },
            )

            return await ctx.send(
                embed=embeds.Embed(
                    description=f"{self.bot.checkmark} Successfully set the starboard channel to {channel.mention}"
                )
            )

        current_ch = discord.utils.get(ctx.guild.channels, id=starboard["channel"])
        await self.bot.mongodb.guilds.update_one(
            {"_id": ctx.guild.id}, {"$set": {"starboard.channel": channel.id}}
        )
        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.checkmark} Successfully changed the starboard channel from {current_ch.mention} to {channel.mention}"
            )
        )

    @commands.has_permissions(manage_channels=True)
    @commands.cooldown(rate=1, per=5, type=commands.BucketType.guild)
    @star_settings.command(
        name="colour",
        aliases=("color", "cl"),
        brief="Change the starboard embed colour",
    )
    async def star_colour(self, ctx, colour: ColourConverter = None):
        """
        Change the colour of the starboard posts by passing a colour value in hex or pure digit form 
        or even with some standard colour names.
        Pass nothing to reset the colour to gold (`#f1c40f`)
        """
        if colour is None:
            colour = discord.Colour.gold()

        starboard = (
            await self.bot.mongodb.guilds.find_one(
                {"_id": ctx.guild.id}, {"starboard": 1}
            )
        ).get("starboard")
        if starboard is None:
            return await ctx.send(
                embed=embeds.Embed(
                    description=(
                        "The starboard hasn't been setup yet."
                        f"Use the channel command to initialize it (`{ctx.prefix}star st ch <channel>`)"
                    ),
                    colour=discord.Colour.red(),
                    timestamp=None,
                )
            )

        old_hue = starboard["colour"]
        await self.bot.mongodb.guilds.update_one(
            {"_id": ctx.guild.id}, {"$set": {"starboard.colour": colour.value}}
        )
        await ctx.send(
            embed=embeds.Embed(
                description=f"{self.bot.checkmark} Successfully changed the starboard colour from {old_hue} to {colour.value}",
                colour=colour,
            )
        )


def setup(bot):
    bot.add_cog(Starboard(bot))
