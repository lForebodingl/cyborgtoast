#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
from asyncio import sleep as asleep
from datetime import datetime
import discord
from discord.ext.commands import Cog
from libneko import commands, embeds
from secrets import token_hex, choice

from toaster.utils import (
    with_category,
    TimeConverter,
    MessageConverter,
    rand_hex,
    pretty_list,
    GiveawayManager
)


@with_category()
class Giveaways:
    """
    Giveaway fields:
        • Winners
        • Prize
        • Channel ID
        • Message ID
    """

    GIVEAWAY_EMOJI = "\N{PARTY POPPER}"
    GIVEAWAY_HEADER = f"{GIVEAWAY_EMOJI} **GIVEAWAY** {GIVEAWAY_EMOJI}"
    ENDED_COLOUR = 0x3C396B
    MAX_WINNERS = 15
    GW_EXPIRATION = 604_800  # 1 week
    MAX_GW_PER_GUILD = 20  # Plenty giveaways imo

    __NL = "\n"

    def __init__(self, bot):
        self.bot = bot
        self.manager = GiveawayManager(bot)

        # Fire up the check worker in the background
        self.bot.loop.create_task(self.manager.check_giveaways())

    async def terminate_giveaway(self, giveaway):
        """
        Ends a giveaway by rolling out the winner(s) and editing the message
        """
        ch = self.bot.get_channel(int(giveaway["channel"]))
        if not ch:
            return

        try:
            m = await ch.get_message(int(giveaway["message"]))
        except discord.errors.NotFound:
            return

        if m.reactions:
            try:
                contestants = (
                    await (discord.util.get(m.reactions, emoji=self.GIVEAWAY_EMOJI))
                    .users()
                    .flatten()
                )
            except AttributeError:
                await m.add_reaction(self.bot.crossmark)

            winners = set()
            for _ in range(int(giveaway["winners"])):
                c = choice(contestants)
                if c not in winners:
                    winners.add(choice(contestants))

            emb = embeds.Embed(
                title=giveaway["prize"],
                description=(
                    "**Winners:**\n"
                    f"{pretty_list(map(lambda m: m.mention, winners), sep=self.__NL)}"
                ),
                colour=self.ENDED_COLOUR,
            )

            emb.set_footer(text="Ended at")

            try:
                await m.edit(embed=emb)
            except (discord.Forbidden, discord.errors.NotFound):
                self.bot.logger.error("Tried ending a giveaway but couldnt!")

    async def check_giveaways(self):
        while True:  # Gonna be checking as long as the bot is on
            active_gw = await self.bot.redis.smembers("giveaways:index")
            now = datetime.utcnow().timestamp()

            for t in map(int, active_gw):
                if t < now:
                    continue

                key = f"giveaways:{t}"

                gw = await self.bot.redis.hgetall(key)

                if not gw:
                    continue

            try:
                await self.terminate_giveaway(gw)
            except Exception as ex:
                self.bot.logger.error(ex, exc_info=True)
            else:
                await self.bot.redis.delete(key)
                await self.bot.redis.srem(t)

            await asleep(5)

    def is_giveaway(self, message: discord.Message) -> bool:
        if not message.author == self.bot.user and message.content.startswith(
            self.GIVEAWAY_HEADER
        ):
            return True

        return False

    @commands.has_permissions(manage_messages=True)
    @commands.cooldown(rate=1, per=15, type=commands.BucketType.guild)
    @commands.group(name="giveaway", aliases=("gw",), brief="Easily host giveaways")
    async def giveaway_group(self, ctx):
        await ctx.invoke(self.create_giveaway)

    @giveaway_group.group(
        name="create", aliases=("c",), brief="Interactive giveaway creation"
    )
    async def create_giveaway(self, ctx):
        ...

    @giveaway_group.group(name="start", aliases=("s",), brief="Start a new giveaway")
    async def start_giveaway(
        self,
        ctx,
        time: TimeConverter,
        winners: int = 1,
        *,
        prize: str = "Something amazing!",
    ):
        ...

    @giveaway_group.group(
        name="end", aliases=("e",), brief="End a giveaway prematurely"
    )
    async def end_giveaway(self, ctx, giveaway: MessageConverter = None):
        if giveaway is None:
            guild_index = await self.bot.redis.smembers(
                f"giveaways:guilds:{ctx.guild.id}"
            )

            if not guild_index:
                raise UserWarning(
                    f"{self.bot.crossmark} There are no running giveaways at the moment!"
                )

            # Sorting the timestamps and getting the oldest one
            last_gw = sorted(map(int, guild_index))[0]

        if not self.is_giveaway(giveaway):
            raise UserWarning(f"{self.bot.crossmark} That's not a giveaway message!")

        gw_timestamp = await self.bot.redis.hget("giveaway:msgindex", giveaway.id)

        if not gw_timestamp:
            raise UserWarning(f"{self.bot.crossmark} That giveaway has already ended!")

    @giveaway_group.group(
        name="reroll", aliases=("rr",), brief="Reroll a giveaway's winner"
    )
    async def reroll_giveaway(self, ctx, giveaway: MessageConverter = None):
        if giveaway is None:
            ...

        if not self.is_giveaway(giveaway):
            raise UserWarning(f"{self.bot.crossmark} That's not a giveaway message!")

    @giveaway_group.group(
        name="list", aliases=("ls",), brief="Lists all the currently running giveaways"
    )
    async def list_giveaways(self, ctx):
        async with ctx.typing():
            guild_giveaways = await self.bot.redis.smembers(
                f"giveaways:guilds:{ctx.guild.id}"
            )

            emb = embeds.Embed(title="Giveaways List")

            if not guild_giveaways:
                emb.description = (
                    "`There're no currently running giveaways on this server!`"
                )
            else:
                ...

        await ctx.send(embed=emb)


def setup(bot):
    bot.add_cog(Giveaways(bot))
