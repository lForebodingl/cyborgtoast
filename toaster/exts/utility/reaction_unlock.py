#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

from discord.ext.commands import Cog

class ReactionServerUnlock(Cog):
    """
    Setups a message to which newcomers have to react to in order to
    get access to the whole server
    """
    def __init__(self, bot):
        self.bot = bot

    # @commands.group()
    # async def 


def setup(bot):
    bot.add_cog(ReactionServerUnlock(bot))
    