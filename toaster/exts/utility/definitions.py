#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

import discord
from discord.ext.commands import Cog
from libneko import commands

import aiohttp
from libneko import embeds, pag

from toaster.utils import with_category, trunc, pretty_list

UD_DEFINE_URL = "http://api.urbandictionary.com/v0/define"
UD_RANDOM_URL = "http://api.urbandictionary.com/v0/random"
UD_REPLACEMENTS = {"https://": "", "http://": "", "[": "**", "]": "**"}


@with_category()
class Definitions:
    def __init__(self, bot):
        self.bot = bot

    @property
    def client_session(self):
        try:
            cs = getattr(self, '__client_session')
        except AttributeError:
            cs = aiohttp.ClientSession()
            setattr(self, '__client_session', cs)
        finally:
            return cs

    @staticmethod
    def _process_ud_resp(resp: dict):
        pages = []

        for phrase in resp:
            definition = trunc(phrase["definition"], 2000)
            for kw in UD_REPLACEMENTS:
                definition = definition.replace(kw, UD_REPLACEMENTS[kw])
            example = (phrase["example"]).replace("`", "’")
            result_embed = embeds.Embed(
                title=phrase["word"],
                description=trunc(definition),
                url=phrase["permalink"],
            )
            result_embed.set_author(
                name=f"{phrase['author']} (\N{THUMBS UP SIGN} {phrase['thumbs_up']} | \N{THUMBS DOWN SIGN} {phrase['thumbs_down']})"
            )
            if example:
                example = "`" + trunc(example, 1000) + "`"
                result_embed.add_field(name="Example", value=example)
            if "tags" in phrase and phrase["tags"]:
                tags = pretty_list(sorted({*phrase["tags"]}), sep=", ")
                result_embed.add_filed(name="Tags", value=trunc(tags))
            pages.append(result_embed)

        return pages

    @commands.cooldown(rate=1, per=5, type=commands.BucketType.channel)
    @commands.command(
        name="urbandictionary",
        aliases=["ud", "urband"],
        brief="Seach with UrbanDictionary",
    )
    async def urban_dictionary_search(self, ctx, *, query: str = None):
        """Define something with UrbanDictionary, or just get a random definition because why not?"""
        async with ctx.typing():
            if query:
                async with self.client_session.get(
                    UD_DEFINE_URL, params={"term": query}
                ) as resp:
                    resp.raise_for_status()
                    resp = (await resp.json())["list"]
            else:
                async with self.client_session.get(UD_RANDOM_URL) as resp:
                    resp.raise_for_status()
                    resp = (await resp.json())["list"]

            if not resp:
                raise UserWarning(
                    ":question: Are you sure that's a thing? Because I can't find it."
                )

            pages = self._process_ud_resp(resp)

        pag.navigator.EmbedNavigator(pages=pages, ctx=ctx).start()


def setup(bot):
    bot.add_cog(Definitions(bot))
