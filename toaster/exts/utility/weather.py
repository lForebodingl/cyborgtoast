#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

import discord
from discord.ext.commands import Cog
from libneko import commands
from urllib.parse import quote as url_quote
import bs4
import aiohttp

from toaster.utils import with_category, closable

def url(arg):
    return f"http://wttr.in/{url_quote(arg[:30])}?T2qn"

@with_category()
class WeatherCog:
    def __init__(self, bot):
        self.bot = bot

    @commands.cooldown(rate=1, per=30, type=commands.BucketType.channel)
    @commands.command(name="weather", brief="Check the weather :D")
    async def wttr_weather(self, ctx, *, query: url = url("Lisbon")):
        self.bot.logger.info(query)
        async with ctx.typing():
            async with aiohttp.ClientSession() as session:
                async with session.get(query) as resp:
                    try:
                        resp.raise_for_status()
                    except aiohttp.client_exceptions.ClientResponseError as ex:
                        if ex.code == 404 and ex.message == "NOT FOUND":
                            raise UserWarning(f"{self.bot.crossmark} That's an invalid location!")
                    text = await resp.text()

            soup = bs4.BeautifulSoup(text, features="html.parser")
            data = soup.find(name="body").find("pre").text

        report = await ctx.send(f"```scala\n{data}\n```\n")

        self.bot.loop.create_task(closable(self.bot, (ctx.message, report), ctx.author))

def setup(bot):
    bot.add_cog(WeatherCog(bot))
