#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
Tags/Notes made easy

This is my aproach at a tag system like R. Danny's one.
It uses MongoDB instead of SQL and currently has no global box feature.
"""

import discord
from discord.ext.commands import Cog
from libneko import commands

from libneko import embeds, converters
from libneko.pag import navigator
from toaster.utils import pretty_list, with_category, TagManager, success_embed, GlobalUserConverter

# from toaster.utils.redis import RedisPool
import typing


TAG_NAME_MAX_CHARS = 50


class TagName(converters.clean_content):
    def __init__(self, *, lower: bool = False):
        self.lower = lower
        super().__init__()

    async def convert(self, ctx, argument):
        converted = await super().convert(ctx, argument)
        lower = converted.lower().strip()

        if not lower:
            raise commands.BadArgument("Missing tag name.")

        if len(lower) > TAG_NAME_MAX_CHARS:
            raise commands.BadArgument(
                f"Tag name is a maximum of {TAG_NAME_MAX_CHARS} characters."
            )

        first_word, _, _ = lower.partition(" ")

        # get tag command.
        root = ctx.bot.get_command("tag")
        if first_word in root.all_commands:
            raise commands.BadArgument("This tag name starts with a reserved word.")

        return converted if not self.lower else lower


@with_category()
class Tags:
    """
    Tags/Notes made easy
    """

    def __init__(self, bot):
        self.bot = bot

        bot.loop.create_task(self.start_manager())

    async def start_manager(self):
        await self.bot.wait_until_ready()
        self.manager = TagManager(self.bot.mongodb)

    # @staticmethod
    # async def get_tag_list(
    #     ctx, obj: typing.Union[discord.Member, discord.Guild]
    # ) -> typing.List[embeds.Embed]:
    #     """Returns a list of embeds with the guild tags or the tags from a member"""


    #         page = embeds.Embed(
    #             title="Here're all the available tags/notes for this guild!",
    #             description=description,
    #             colour=rand_hex,
    #             timestamp=None,
    #         )
    #         page.set_footer(
    #             icon_url=ctx.guild.icon_url,
    #             text=f"You can get a tag by doing {ctx.prefix}tag <name>",
    #         )

    #         pages.append(page)

    @commands.group(
        name="tag",
        aliases=("note", "t"),
        brief="Tags/Notes made easy",
        case_insensitive=True,
        invoke_without_command=True,
    )
    async def tags_group(self, ctx, name: TagName = None):
        await ctx.invoke(self.get_tag, query= name)

    @tags_group.command(name="list", aliases=("ls",), brief="Lists the requested tags")
    async def list_tags(self, ctx, member: discord.Member = None):
        """
        Lists all the available tags for this guild.
        If you specify a member you'll get a list of all their tags/notes.
        """
        raise NotImplementedError("Not yeet")
        # await ctx.trigger_typing()

        # pages = await self.get_tag_list(ctx, member or ctx.guild)

        # nav = navigator.EmbedNavigator(pages=pages, ctx=ctx)
        # nav.start()

    @tags_group.command(
        name="stats", aliases=("st", "leaderboard", "lb"), brief="See who creates and uses more tags!"
    )
    async def tag_stats(self, ctx, member: discord.Member = None):
        raise NotImplementedError("Not yeet")

        if member is None:
            ...

        ...

    @tags_group.command(name="info", aliases=("in",), brief="Check some info about a tag")
    async def tag_info(
        self, ctx, *, name: typing.Union[TagName, discord.Member]
    ):
        tag = self.manager.head_tag(ctx.guild.id, query=name)

        emb = embeds.Embed(title=f"Stats for `{tag['name']}`")
        emb.add_field(name="Owner", value=tag["owner"], inline=True)
        emb.add_field(name="Uses", value=tag["uses"], inline=True)
        emb.add_field(name="Aliases", value=f"{pretty_list(tag['aliases'], sep='`, `')}")

        await ctx.send(embed=emb)

    @tags_group.command(name="get", aliases=("g",), brief="Get's the given tag")
    async def get_tag(self, ctx, *, name: TagName):
        # Propagated exceptions will be caught
        tag = await self.manager.get_tag(
            ctx.guild.id, query=name
        )  

        await ctx.send(content=tag["content"])

    @tags_group.command(
        name="delete", aliases=("d", "rm", "remove"), brief="Deletes the given tag"
    )
    async def delete_tag(self, ctx, *, name: TagName):
        await self.manager.delete_tag(ctx.guild.id, query=name)

        await success_embed(
            ctx, f"Successfully deleted tag with name/alias **`{name}`**"
        )

    @tags_group.command(
        name="create", aliases=("c", "add"), brief="Creates a brand new tag"
    )
    async def create_tag(self, ctx, name: TagName, *, content: converters.clean_content):
        await self.manager.post_tag(
            ctx.guild.id, name=name, content=content, owner=ctx.author.id
        )

        await success_embed(ctx, f"Successfully created tag with name **`{name}`**")

    @tags_group.command(
        name="edit", aliases=("e",), brief="Creates a brand new tag"
    )
    async def edit_tag(self, ctx, name: TagName, *, content: converters.clean_content):
        await self.manager.patch_tag(ctx.guild.id, query=name, content=content)

        await success_embed(
            ctx, f"Successfully edited tag with name/alias **`{name}`**"
        )

    @tags_group.command(
        name="raw", aliases=("r",), brief="Creates a brand new tag"
    )
    async def raw_tag(self, ctx, name: TagName):
        tag = await self.manager.get_tag(ctx.guild.id, query=name)

        raw_tag = await (converters.clean_content(escape_markdown=True)).convert(
            ctx, tag["content"]
        )

        await ctx.send(content=raw_tag)

    @tags_group.command(
        name="purge", aliases=("p",), brief="Creates a brand new tag"
    )
    async def purge_tags(self, ctx, user: GlobalUserConverter):
        await self.manager.delete_tag(ctx.guild.id, query=user.id)

        await success_embed(
            ctx, f"Successfully purged all tags from {user}"
        )

    @tags_group.command(
        name="alias", aliases=("a",), brief="Creates a brand new tag"
    )
    async def alias_tag(
        self, ctx, name: TagName, alias: TagName
    ):
        await self.manager.patch_tag(ctx.guild.id, query=name, alias={"add": alias})

        await success_embed(
            ctx, f"Successfully created alias **`{alias}`** that points to **`{name}`**"
        )

    @tags_group.command(
        name="unalias", aliases=("ua",), brief="Creates a brand new tag"
    )
    async def unalias_tag(
        self, ctx, alias: TagName
    ):
        await self.manager.patch_tag(ctx.guild.id, query=alias, alias={"rem": alias})

        await success_embed(
            ctx, f"Successfully removed alias **`{alias}`**"
        )


def setup(bot):
    bot.add_cog(Tags(bot))
