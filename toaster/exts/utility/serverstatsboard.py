#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""
A little demographics board
Inspired in the server stats board present in 
"""

import discord
from discord.ext.commands import Cog
from libneko import commands
from libneko import embeds
from typing import List, Dict, Union, Optional

from toaster.utils import (
    chk_or_crs,
    rand_hex,
    titlefy,
    pretty_list,
    confirm_opts,
    with_category,
)


POSSIBLE_FIELDS = {
    "ID": lambda g: g.id,
    "OWNER": lambda g: g.owner,
    "INVITE": lambda g: g.invites(),
    "MEMBER COUNT": lambda g: g.member_count,
    "BOT COUNT": lambda g: len({g for g in g.members if g.bot}),
    "HUMAN COUNT": lambda g: POSSIBLE_FIELDS["MEMBER COUNT"](g)
    - POSSIBLE_FIELDS["BOT_COUNT"](g),
    "REGION": lambda g: titlefy(str(g.region)),
    "VERIFICATION": lambda g: titlefy(str(g.verification_level)),
    "CONTENT FILTER": lambda g: titlefy(str(g.explicit_content_filter)),
    "MFA LEVEL": lambda g: "ON" if g.mfa_level else "OFF",
    "AFK CHANNEL": lambda g: g.afk_channel,
    "AFK TIMEOUT": lambda g: g.afk_timeout
    / 60,  # Min. is 1 minute so there's no need to have secs
    "CHANNEL COUNT": lambda g: len(g.channels),
    "TC COUNT": lambda g: len(g.text_channels),
    "VC COUNT": lambda g: len(g.voice_channels),
    "BAN COUNT": lambda g: g.bans(),  # Custom handling later on, I guess
    "ROLE COUNT": lambda g: len(g.roles),
    "FEATURES": lambda g: ", ".join(map(titlefy, sorted(g.features, key=str))),
}

DEFAULT_FIELDS = ("MEMBER COUNT", "INVITE")


def upper(arg: str) -> str:
    return str(arg).upper()


@with_category()
class ServerStatsBoard:
    """
    The cog holding the demographic board commands
    """

    def __init__(self, bot):
        self.bot = bot
        # The cache for the board changes.
        # Should have this format:
        # {
        #     1234: {
        #     "add": {1: "ID"},
        #     "remove": [...]
        #     },
        #     ...
        # }
        # Where '1234' is a guild id and '1' is the position
        self.changes_cache = {}

    @staticmethod
    def _calc_changes(
        old_f: List[str], new_f: Dict[str, Union[Dict[str, str], List[str]]]
    ) -> List[str]:
        changed_fields = old_f

        for p, c in new_f["add"]:
            changed_fields.insert(p, c)

        for c in new_f["remove"]:
            changed_fields.remove(c)

        return changed_fields

    @staticmethod
    async def _homogenize_board(
        ssb: Dict[str, Union[List[str], int]],
        board: discord.CategoryChannel,
        fields: List[str],
    ) -> None:
        ch_gen = (
            c
            for c in board.channels
            if isinstance(c, discord.VoiceChannel)
            and c.name.upper().startswith(tuple(fields + ["https://discord.gg"]))
        )

    @staticmethod
    async def _update_board(
        ctx, board: discord.CategoryChannel, fields: List[str]
    ) -> None:
        to_add = len(fields) - len(board.channels)  # or to remove ;)
        ...

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.channel)
    @commands.group(
        name="serverstatsboard",
        aliases=("ssb",),
        brief="Setup a board with some server stats",
        invoke_without_command=True,
    )
    async def stats_board(self, ctx):
        await self.board_overview(self, ctx)

    @commands.cooldown(rate=1, per=10, type=commands.BucketType.channel)
    @stats_board.command(
        name="overview",
        aliases=("ov",),
        brief="Get an overview of the current settings",
    )
    async def board_overview(self, ctx):
        ssb = await self.bot.mongodb.ssb.find_one({"_id": ctx.guild.id})
        if ssb is None:
            raise UserWarning(
                f"{self.bot.crossmark} The statsboard hasn't been setup yet! "
                f"Add at least one field (`{ctx.prefix}ssb add <field>`) and then "
                f"apply the changes (`{ctx.prefix}ssb apply`) to initialize the board"
            )

        emb = embeds.Embed(
            title=":gear: StatsBoard overview",
            description="Here're the current stats board settings!",
        )

        emb.add_field(
            name="Stats Tab", value=ctx.guild.get_channel(ssb["board"]).mention
        )

        if ssb["fields"]:
            fields = pretty_list(map(lambda f: f"• `{f}`", ssb["fields"]), sep="\n")
        else:
            fields = f"`No active fields yet. Start by adding fiedls with '{ctx.prefix}ssb add <field>' and th"
        emb.add_field(name=":clipboard: Displayed Fields", value=fields, inline=False)

        await ctx.send(embed=emb)

    @commands.has_permissions(manage_channels=True)
    @commands.bot_has_permissions(manage_channels=True)
    @commands.cooldown(rate=1, per=15, type=commands.BucketType.guild)
    @stats_board.command(
        name="apply", aliases=("commit", "ap", "c"), brief="Apply the recorded changes"
    )
    async def board_apply(self, ctx):
        try:
            cached_changes = self.changes_cache[ctx.guild.id]
        except KeyError:
            raise UserWarning(f"{self.bot.crossmark} There're no stashed changes!")

        ssb = await self.bot.mongodb.ssb.find_one({"_id": ctx.guild.id})
        if ssb is None:
            # Creating the board
            ow = {
                ctx.guild.default_role: discord.PermissionOverwrite(
                    read_messages=True, connect=False, speak=False
                )
            }
            board = await ctx.guild.create_category_channel(
                "Server Stats", overwrites=ow
            )
            fields = self._calc_changes([], self.changes_cache[ctx.guild.id])
            await self._update_board(ctx, board, fields)
            await self.bot.mongodb.ssb.insert_one(
                {"_id": ctx.guild.id, "board": board.id, "fields": fields}
            )
            ...

    @commands.has_permissions(manage_channels=True)
    @commands.bot_has_permissions(manage_channels=True)
    @stats_board.command(name="add", aliases=("a",), brief="Adds a field to the board")
    async def board_add(self, ctx, position: Optional[int] = None, *, field: upper):
        ssb = await self.bot.mongodb.ssb.find_one({"_id": ctx.guild.id})

        if field not in POSSIBLE_FIELDS:
            raise UserWarning(f"{self.bot.crossmark} That isn't a valid field!")

        if field in ssb["fields"]:
            raise UserWarning(
                f"{self.bot.crossmark} That isn't active field already, so I cannot add it again."
            )

        if field in self.changes_cache[ctx.guild.id]["add"]:
            raise UserWarning(
                f"{self.bot.crossmark} That change is already registered!"
            )

        self.changes_cache[ctx.guild.id]["add"][field] = position

        # There's no need to send a full message imo
        await ctx.message.add_reaction(self.bot.checkmark)

    @commands.has_permissions(manage_channels=True)
    @commands.bot_has_permissions(manage_channels=True)
    @stats_board.command(
        name="remove", aliases=("rm",), brief="Removes a field from the board"
    )
    async def board_remove(self, ctx, *, field: upper):
        ssb = await self.bot.mongodb.ssb.find_one({"_id": ctx.guild.id})

        if field not in POSSIBLE_FIELDS:
            raise UserWarning(f"{self.bot.crossmark} That isn't a valid field!")

        if field not in ssb["fields"]:
            raise UserWarning(
                f"{self.bot.crossmark} That isn't an active field, so I cannot remove it."
            )

        if field in self.changes_cache[ctx.guild.id]["remove"]:
            raise UserWarning(
                f"{self.bot.crossmark} That change is already registered!"
            )

        self.changes_cache[ctx.guild.id]["remove"].append(field)

        # There's no need to send a full message imo
        await ctx.message.add_reaction(self.bot.checkmark)

    @commands.has_permissions(manage_channels=True)
    @commands.bot_has_permissions(manage_channels=True)
    @commands.cooldown(rate=1, per=60, type=commands.BucketType.guild)
    @stats_board.command(
        name="reset", aliases=("rs",), brief="Resets the board to the defaults"
    )
    async def board_reset(self, ctx):
        ssb = await self.bot.mongodb.ssb.find_one({"_id": ctx.guild.id})

        emb = embeds.Embed(
            description=f"<:maybe:503143206704250880> Are you sure?",
            colour=discord.Colour.gold(),
        )
        emb.set_footer(text="This will timeout in 10 seconds")

        confirm = await ctx.send(embed=emb)
        result = await confirm_opts(self.bot, confirm, ctx.author)
        await confirm.clear_reactions()  # Clear the reactions nontheless

        if result is None:  # Timed out
            emb.set_footer(text="Timed out...")
            await confirm.edit(embed=emb)

        if result:
            await confirm.edit(
                embed=embeds.Embed(
                    description=f"{self.bot.checkmark} Okay!\n*The operation should take some seconds*",
                    colour=discord.Colour.green(),
                )
            )

            board = ctx.guild.get_channel(ssb["board"])
            await self._update_board(ctx, board, DEFAULT_FIELDS)

        if not result:
            await confirm.edit(
                embed=embeds.Embed(
                    description=f"{self.bot.crossmark} Operation canceled!",
                    colour=discord.Colour.red(),
                )
            )


def setup(bot):
    bot.add_cog(ServerStatsBoard(bot))
