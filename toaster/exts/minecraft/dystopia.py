#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
import discord
from libneko import commands
from libneko.pag import navigator
from libneko import embeds
import asyncio
from toaster.utils import strings, misc
from mcstatus import MinecraftServer
import traceback

# online https://cdn.discordapp.com/attachments/444952275325026324/471009805792968704/online_banner_lower_rez.png
# offline https://cdn.discordapp.com/attachments/444952275325026324/471009804018515971/offline_banner_lower_rez.png


@misc.with_category()
class Dystopia:
    def __init__(self, bot):
        self.bot = bot

    @commands.group(
        name="dystopia",
        aliases=["ds"],
        brief="This group contains some commands realated to the Dystopia Server!",
        case_insensitive=True,
        invoke_without_command=True,
    )
    async def dystopia_group(self, ctx):
        await misc.command_not_found(ctx)

    @dystopia_group.command(
        name="status",
        aliases=["s", "ping"],
        brief="Pings the server and retrieves it's status.",
    )
    async def server_status(self, ctx):
        server = MinecraftServer.lookup("198.20.102.92:25585")
        ping = None
        try:
            ping = server.ping()
            query = server.query()
            img = "https://cdn.discordapp.com/attachments/444952275325026324/471009805792968704/online_banner_lower_rez.png"
        except:
            print(traceback.format_exc())
            img = "https://cdn.discordapp.com/attachments/444952275325026324/471009804018515971/offline_banner_lower_rez.png"
        nl = "\n"
        em = embeds.Embed(title="Dystopia Server Status", timestamp=None)
        em.set_thumbnail(
            url="https://cdn.discordapp.com/attachments/444952275325026324/470961905746575381/server-icon.png"
        )
        em.set_image(url=img)
        if ping:
            em.add_field(name="Latency", value=f"{int(ping)}ms")
            players = query.players
            if players.online:
                value = f"Total: `{query.players.online}`{nl}Names:{nl}:small_blue_diamond:{strings.pretty_list(query.players.names, sep=f'{nl}:small_blue_diamond')}"
            else:
                value = "`There are no players online at this moment`"
            em.add_field(name="Online Players", value=value)
        await ctx.send(embed=em)

    @dystopia_group.command(
        name="banner",
        aliases=["b"],
        brief="Sends the server banner provided by the host",
    )
    async def beastnode_banner(self, ctx):
        em = embeds.Embed(title="Status banner")
        em.set_image(url="https://mc.beastnode.com/index.php?r=status/120785.png")
        await ctx.send(embed=em)


def setup(bot):
    """Defining this extension"""
    bot.add_cog(Dystopia(bot))
