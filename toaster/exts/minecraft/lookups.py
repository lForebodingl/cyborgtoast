#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
import discord
from libneko import commands
from libneko import embeds

from toaster.utils import with_category

@with_category()
class MCLookups:
    def __init__(self, bot):
        self.bot = bot
        bot.loop.create_task(self._finish_setup(bot))

    async def _finish_setup(self, bot):
        async with await bot.baker.rsc.get("minecraft_links.json") as fp:
            self.links = await fp.read().strip()

    @commnads.group(name="minecraft", aliases=("mc",), brief="Various Minecraft related lookups", case_insensitive=True, invoke_without_command=True)
    async def mc_group(self, ctx):                        
        await ctx.send(embed=embeds.Embed(title="Here're some Minecraft related resources!", description=self.links))


    @mc_group.command(name="skin", aliases=("sk",), brief="Lookup a player skin")
    async def mc_skin(self, ctx, player: str = None):
        """Can be UUID or IGN. Can also be empty for DB query"""
        ...

    @mc_group.command(name="player", brief="Gets some player info")
    async def mc_player(self, ctx, player: str = None):
        """Can be UUID or IGN. Can also be empty for DB query"""
        ...


def setup(bot):
    bot.add_cog(MCLookups(bot))
