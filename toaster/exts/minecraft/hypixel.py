#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
import discord
from libneko import commands
from libneko import embeds

from toaster.utils import with_category

@with_category()
class Hypixel:
    def __init__(self, bot):
        self.bot = bot

    @commands.group(name="hypixel", aliases=("hy",), brief="Hypixel related stuffz", case_insensitive=True)
    async def hypixel_group(self, ctx):
        ...

    @hypixel_group.command(name="howtolink", aliases=("htl",), brief="Little guide on how to link Discord with Hypixel")
    async def how_to_link(self, ctx):
        """This will teach you how to link a Discord account to a Hypixel profile!"""
        emb = embeds.Embed(
            title="__**How to link your Discord account with your Hypixel profile**__",
            description="This will teach you how to link your Discord account with your Hypixel profile so that you can use some commands provided by me :smile:",
            url="https://www.wikihow.com/Link-a-Discord-Account-with-a-Hypixel-Profile",
            colour=discord.Colour(2895667),
        )
        emb.set_thumbnail(
            url="https://cdn.discordapp.com/attachments/421358461473783819/435450006749708288/serveimage.jpeg"
        )
        emb.set_footer(
            text="How to link Discord with Hypixel page | For more info click the Hypixel icon.",
            icon_url="https://cdn.discordapp.com/attachments/421358461473783819/437591960874778645/cyborgtoast.png",
        )
        emb.add_field(
            name=":one:",
            value="First login into the Hypixel Network with your Minecraft account.",
        )
        emb.add_field(
            name=":two:",
            value="After Successfully logging in, right-click your player head.",
        )
        emb.add_field(name=":three:", value="Click on Social Media.")
        emb.add_field(name=":four:", value="Left-click on Discord.", inline=False)
        emb.add_field(
            name=":five:",
            value="Paste your Discord tag in chat (don't worry no one will see it). And now you are all done!",
        )
        await ctx.send(embed=emb)

def setup(bot):
    bot.add_cog(Hypixel(bot))
