#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
import discord
from discord.ext.commands import Cog
from libneko import commands
from libneko.pag import navigator
import asyncio

from toaster.utils import with_category


@with_category()
class Settings:
    def __init__(self, bot):
        self.bot = bot

    async def generate_board(self):
        """
        This will generate the settings board by injecting all the commands
        which the callbacks are in the form "*_settings".
        """
        ...
        
    @commands.has_permissions(administrator=True)
    @commands.group(
        name="settings",
        aliases=("st", "s"),
        brief="This group contains many useful bot settings that admins can tweak",
        hidden=True
    )
    async def control_panel(self, ctx):
        ...


def setup(bot):
    bot.add_cog(Settings(bot))
