#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
import discord
from discord.ext.commands import Cog
from libneko import commands

from libneko import embeds
from toaster.utils import get_prefix, rand_hex, with_category
from toaster.utils import strings
from textwrap import dedent


@with_category()
class AboutPages:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        name="about", aliases=("bi", "info"), brief="General info about me!"
    )
    async def about_page(self, ctx):
        """Sends some general useful information about me."""
        raw_prefixes = await get_prefix(self.bot, ctx.message)

        em = embeds.Embed(
            title=":information_source: __CyborgToast about page__",
            description=dedent(
                f"""
            {self.bot.description}

            __**Other useful places**__
            \N{EM SPACE}• `{ctx.prefix}help` - Check all my commands out!
            \N{EM SPACE}• `{ctx.prefix}credits` - The place where I thank all of the people who helped me come to life.
            \N{EM SPACE}• `{ctx.prefix}support` - Get the link for the support Discord server (a bot playground and emoji hoster too)
            \N{EM SPACE}• `{ctx.prefix}stats` - The nerdy stats :nerd:
            \N{EM SPACE}• `{ctx.prefix}updates` - Get the latest updates from my support server

            Here are some more general facts about me.
            """
            ),
            color=discord.Colour.from_rgb(71, 62, 120),
        )
        em.set_footer(
            icon_url=self.bot.user.avatar_url,
            text="Thanks for using CyborgToast! | Made with ❤ by Tmpod#1004",
        )
        em.set_thumbnail(url=self.bot.user.avatar_url)
        em.set_image(url="https://image.ibb.co/dAvgiV/hey-there-i-m-cyborgtoast-1.gif")
        em.add_field(name="__Country__", value="Portugal :flag_pt:", inline=True)
        em.add_field(
            name="__Created on__",
            value=self.bot.user.created_at.strftime("%x at %X"),
            inline=True,
        )
        em.add_field(
            name="__Users Count__", value=str(len(self.bot.users)), inline=True
        )
        em.add_field(
            name="__Guild Count__", value=str(len(self.bot.guilds)), inline=True
        )
        heartbeat = (
            self.bot.latency or (sum(self.bot.latencies) / len(self.bot.latencies))
        ) * 1000
        em.add_field(name="__Ping__", value=f"{heartbeat:,.2f}ms", inline=True)
        em.add_field(
            name="__Uptime__", value=f"{str(self.bot.uptime)[:-7]}h", inline=True
        )
        em.add_field(name="__Calories__", value=await self.bot.size(), inline=True)

        if ctx.guild:
            em.add_field(
                name="__Prefix(es) for this guild__",
                value=f"`{strings.pretty_list(raw_prefixes, sep='`, `')}`",
            )

        await ctx.send(embed=em)

        # em.add_field(name="__CPU__", value=f"Cores: {psutil.cpu_count()}\nUsage: {psutil.cpu_percent(interval=None)}%")
        # em.add_field(name="__RAM__", value=f"Usage: {psutil.virtual_memory()[2]}%")

    @commands.command(name="stats", brief="Check my current system stats")
    async def stats_page(self, ctx, options: str = None):
        """
        Shows the current system stats, such as CPU and RAM usage, event loop and function callback latency, package versions, etc.
        Append the `--full` or `-f` option to see the full stats page
        """
        await ctx.send("Not done yet :wink:")

    @commands.command(aliases=["sp"], brief="Sends my support server's invite link.")
    async def support(self, ctx):
        """Sends the link for my support server. Feel free to ask anything there if you have an issue with my services."""
        await ctx.send(
            embed=embeds.Embed(
                title=":tools: Here's my support server!",
                description=self.bot.config.toast.support_server,
                colour=rand_hex(),
            )
        )

    @commands.command(name="updates", brief="Read my latest update messages")
    async def updates_page(self, ctx):
        await ctx.trigger_typing()

        upd_ch = self.bot.get_channel(self.bot.config.toast.channels.updates)
        upc_ch = self.bot.get_channel(self.bot.config.toast.channels.upcoming)

        async for msg in upd_ch.history(limit=1):
            if msg.embeds:
                upd_msg = msg

        async for msg in upc_ch.history(limit=1):
            if msg.embeds:
                upc_msg = msg

        emb = embeds.Embed(
            title="Latest Updates and Upcoming messages",
            description=f"Do `{ctx.prefix}support` to get the link for my support server where this messages are posted!",
        )
        emb.set_footer(
            text="Thanks for using CyborgToast! | Made with ❤️ by Tmpod#1004"
        )
        emb.add_field(
            name="Updates",
            value=strings.embed_to_str(upd_msg.embeds[0]) + "\n\N{ZERO WIDTH SPACE}",
        )
        emb.add_field(name="Upcoming", value=strings.embed_to_str(upc_msg.embeds[0]))

        await ctx.send(embed=emb)

    @commands.command(name="credits", aliases=["crd"], brief="Shows the credits page.")
    async def credits_page(self, ctx):
        """Show a page where I give credits to those of helped me or that I took inspiration from."""
        em = embeds.Embed(
            title="**CyborgToast Credits**",
            description="Here I give credit to those who helped this bot come to life.",
            colour=discord.Colour.dark_green(),
        )
        em.set_thumbnail(url=self.bot.user.avatar_url)
        em.set_footer(text="Thanks for using CyborgToast! | Made with ❤️ by Tmpod#1004")
        async with await self.bot.baker.rsc.request("GET", "credits.txt") as fp:
            r_msg = await fp.read()

            fields = r_msg.split("===")
        for f in fields:
            f_bits = f.split("---")
            em.add_field(name=f_bits[0], value=f_bits[1])
        await ctx.send(embed=em)

    @commands.command(
        name="datacollection",
        aliases=("datacoll",),
        brief="Explains the data collection policy.",
    )
    async def data_collection(self, ctx):
        async with await self.bot.baker.rsc.request("GET", "data_collection.txt") as fp:
            data_coll = (await fp.read()).strip()

        await ctx.send(
            embed=embeds.Embed(
                title="Here's my data collection policy!", description=data_coll
            )
        )

    @commands.command(name="donate", aliases=("patreon",), brief="Show me your ❤️!")
    async def donnate_command(self, ctx):
        await ctx.send(
            embed=embeds.Embed(
                title="Here's my Donation page!",
                description=(
                    f"You can show me your love by joining my "
                    "patreon page over [here](https://patreon.com/tmpod)!\n"
                    "**Thank you!** ❤️"
                ),
                colour=rand_hex()
            )
        )


def setup(bot):
    bot.add_cog(AboutPages(bot))
