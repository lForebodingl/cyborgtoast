#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
__all__ = ("TagManager",)

from datetime import datetime
from libneko.logging import Log
from typing import Union

# from . import MongoDatabase

# TODO: Add docs


class TagManager(Log):
    """
    Manages the tag database operations.

    Current schema:

    [                        # Collection
      {                      # Document
        "_id": 1234,
        "tags": [
          {
            "name": "meow",
            "owner": 5678,
            "created_at": 69,
            "uses": 0,
            "aliases": [],
            "content": "I wanna be a cat",
          }
        ]
      }
    ]
    """

    VALID_METHODS = ("HEAD", "GET", "POST", "PATCH", "DELETE", "TRACE")

    def __init__(self, db, *, tags_coll: str = "tags"):
        self.db = db
        self.tags_coll = tags_coll

    async def request(self, method: str, guild_id: int, **payload):
        if not isinstance(method, str):
            raise TypeError("The method must be a string!")

        if not isinstance(guild_id, int):
            raise TypeError("The guild ID must be a string!")

        if method.upper() not in self.VALID_METHODS:
            raise ValueError(f"{method} is an invalid method!")

        self.log.debug("Requested %s with payload: %s", method, payload)
        await (getattr(self, f"{method.lower()}_tag"))(guild_id, **payload)

    async def head_tag(self, guild_id: int, *, query: str) -> dict:
        self.log.debug("Searching for tag with name/alias `%s`...", query)
        result = await self.db[self.tags_coll].find_one(
            {
                "_id": guild_id,
                "$or": [{"tags.name": query}, {"tags.aliases": {"$in": [query]}}],
            }
        )

        if not result:
            self.log.debug("Couldn't find tag with name/alias: %s", query)
            raise RuntimeError(f"Tag with name/alias `{query}` wasn't found")

        self.log.debug("Returning %s from GET", result)
        return result

    async def get_tag(self, guild_id: int, *, query: str) -> dict:
        """
        This just wraps the HEAD method with the incrementing of the uses counter
        """
        # if not isinstance(query, str):  # Not really necessary, is it...
        #     raise TypeError("Tag query must be a string!")
        result = await self.head_tag(guild_id, query=query)

        self.log.debug("Upping uses counter...")
        await self.db[self.tags_coll].update_one(
            {
                "_id": guild_id,
                "$or": [{"tags.name": query}, {"tags.aliases": {"$in": [query]}}],
            },
            {"$inc": {"tags.$.uses": 1}},
        )

        self.log.debug("Returning %s from GET", result)
        return result

    async def post_tag(
        self, guild_id: int, *, name: str, content: str, owner: int
    ) -> str:
        self.log.debug("Checking if name/alias %s already exists", name)
        search = await self.db[self.tags_coll].find_one(
            {
                "_id": guild_id,
                "$or": [{"tags.name": name}, {"tags.aliases": {"$in": [name]}}],
            }
        )

        if search:
            self.log.debug("Found tag with name/alias `%s`", name)
            raise RuntimeError("Tag with name/alias `{name}` already exists!")

        self.log.debug("Tag name is free! Posting data...")

        # We are good to go
        return await self.db[self.tags_coll].update_one(
            {"_id": guild_id},
            {
                "$addToSet": {
                    "tags": {
                        "name": name,
                        "owner": owner,
                        "created_at": datetime.utcnow().timestamp(),
                        "content": content,
                        # no need to add "uses" and "aliases" as later they get created if needed
                    }
                }
            },
        )

    async def patch_tag(
        self,
        guild_id: int,
        *,
        query: str,
        name: str = None,
        content: str = None,
        alias: dict = None,
    ) -> str:
        self.log.debug("Searching for a tag with name/alias `%s`", name)
        result = await self.db[self.tags_coll].find_one(
            {
                "_id": guild_id,
                "$or": [{"tags.name": query}, {"tags.aliases": {"$in": [query]}}],
            }
        )

        if not result:
            self.log.debug("Couldn't find tag with name/alias `%s`", name)
            raise RuntimeError("Tag with name/alias `{query}` doesn't exist!")

        self.log.debug("Found tag! Updating data...")

        if name:
            self.log.debug("Setting name to `%s`", name)
            await self.db[self.tags_coll].update_one(
                {
                    "_id": guild_id,
                    "$or": [{"tags.name": query}, {"tags.aliases": {"$in": [query]}}],
                },
                {"$set": {"tags.$.name": name}},
            )

        if content:
            self.log.debug("Setting content to `%s`", content)
            await self.db[self.tags_coll].update_one(
                {
                    "_id": guild_id,
                    "$or": [{"tags.name": query}, {"tags.aliases": {"$in": [query]}}],
                },
                {"$set": {"tags.$.content": content}},
            )

        if alias:
            to_set = {}
            if alias.get("add"):
                self.log.debug("Adding alias `%s`", alias)
                to_set["$addToSet"] = {"tags.$.alias": alias}

            if alias.get("rem"):
                self.log.debug("Removing alias `%s`", alias)
                to_set["$pull"] = {"tags.$.alias": alias}

            await self.db[self.tags_coll].update_one(
                {
                    "_id": guild_id,
                    "$or": [{"tags.name": query}, {"tags.aliases": {"$in": [query]}}],
                },
                {"$set": {to_set}},
            )

    async def delete_tag(self, guild_id: int, *, query: Union[str, int]) -> str:
        if isinstance(query, int):
            self.log.debug("Purging all tags from member with ID: `%s`", query)
            return await self.db[self.tags_coll].update_one(
                {"_id": guild_id}, {"$pull": {"tags": {"owner": query}}}
            )

        self.log.debug("Searching for a tag with name/alias `%s`", query)
        result = await self.db[self.tags_coll].find_one(
            {
                "_id": guild_id,
                "$or": [{"tags.name": query}, {"tags.aliases": {"$in": [query]}}],
            }
        )

        if not result:
            self.log.debug("Couldn't find tag with name/alias `%s`", query)
            raise RuntimeError("Tag with name/alias `{query}` doesn't exist!")

        self.log.debug("Found tag! Deleting it...")

        await self.db[self.tags_coll].update_one(
            {"_id": guild_id},
            {
                "$pull": {
                    "tags": {
                        "$or": [
                            {"tags.name": query},
                            {"tags.aliases": {"$in": [query]}},
                        ]
                    }
                }
            },
        )

    async def trace_tags(self, guild_id: int):
        self.log.debug("Aggregating tag names and aliases for guild with ID: `%s`", guild_id)

        return await self.db[self.tags_coll].aggregate(
            {"$match": {"_id": guild_id}},
            {"$project": {"_id": 0, "name": 1, "aliases": 1}},
        )
