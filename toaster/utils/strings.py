#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
__all__ = (
    "trunc",
    "pretty_list",
    "plur_simple",
    "plur_diff",
    "is_url",
    "titlefy",
    "to_upper",
    "to_lower",
    "fake_emb_field",
    "embed_to_str",
)

from discord.embeds import _EmptyEmbed
import urllib
from libneko import embeds
from textwrap import dedent
from datetime import datetime


def trunc(string: str, limit: int = 2000) -> str:
    return string if len(string) < limit else string[: limit - 3] + "..."


def _sync_pretty_list(iterable, sep, p_end, to_s) -> str:
    iterable = list(iterable)
    if p_end:
        last = iterable.pop(-1)
        return f"{sep.join(map(to_s, iterable))}{p_end}{last}"
    return sep.join(map(to_s, iterable))


async def _async_pretty_list(async_iterable, sep, p_end, to_s) -> str:
    iterable = []
    async for item in async_iterable:
        iterable.append(item)
    return _sync_pretty_list(iterable, sep, p_end, to_s)


def pretty_list(iterable, *, sep: str = ", ", p_end: str = None, to_s=str) -> str:
    """Prettyfies iterables with the give seperator"""
    if hasattr(iterable, "__anext__"):
        return _async_pretty_list(iterable, sep, p_end, to_s)

    return _sync_pretty_list(iterable, sep, p_end, to_s)


def plur_simple(cardinality: int, word: str, suffix="s") -> str:
    """Pluralises words that just have a suffix if pluralised."""
    if cardinality - 1:
        word += suffix
    return f"{cardinality} {word}"


def plur_diff(cardinality: int, singular: str, plural: str) -> str:
    """Pluralises words that change spelling when pluralised."""
    return f"{cardinality} {plural if cardinality - 1 else singular}"


def is_url(url: str) -> bool:
    """Return true if the string is a valid URL. False otherwise."""
    try:
        urllib.parse.urlparse(url)
        return True
    except Exception:
        return False


def titlefy(val: str) -> str:
    """Replaces 'space marking' chars with actual spaces and makes the string title-like"""
    # Forcing the string :/
    return str(val).title().replace("_", " ").replace("-", " ").replace(".", " ")


def to_upper(arg: str) -> str:
    """Makes a string upper cased. Used for cmd arg converters"""
    return arg.upper()


def to_lower(arg: str) -> str:
    """Makes a string lower cased. Used for cmd arg converters"""
    return arg.lower()


def fake_emb_field(name: str, value: str) -> str:
    """Makes a fake embed field"""
    return f"**{name}**\n{value}"


def embed_to_str(emb: embeds.Embed) -> str:
    """Converts an embed object into a string"""
    try:
        if not isinstance(emb.author.name, _EmptyEmbed):
            author = f"*{emb.author.name}*"
        else:
            author = ""
    except AttributeError:
        author = ""

    try:
        if not isinstance(emb.tile, _EmptyEmbed):
            title = f"**{emb.title}**"
        else:
            title = ""
    except AttributeError:
        title = ""

    try:
        if not isinstance(emb.description, _EmptyEmbed):
            desc = emb.description
        else:
            desc = ""
    except AttributeError:
        desc = ""

    result = dedent(
        f"""
    {author}
    {title}
    {desc}
    """
    )

    for f in emb.fields:
        result += dedent(
            f"""

            **{f.name}**
            {f.value}
            """
        )

    try:
        timestamp = emb.timestamp
    except AttributeError:
        final_stamp = ""
    else:
        if isinstance(timestamp, _EmptyEmbed):
            final_stamp = ""
        else:
            now = datetime.utcnow()
            if timestamp.strftime("%y %m") == now.strftime("%y %m"):
                if timestamp.strftime("%d") == now.strftime("%d"):
                    day = "Today"
                elif int(timestamp.strftime("%d")) == int(now.strftime("%d")) - 1:
                    day = "Yesterday"
                else:
                    day = timestamp.strftime("%x")
            else:
                day = timestamp.strftime("%x")
            final_stamp = f"`{day} at {timestamp.strftime('%X')}`"

    try:
        if not isinstance(emb.footer.text, _EmptyEmbed):
            footer = f"{emb.footer.text} • "
        else:
            footer = ""
    except AttributeError:
        footer = ""
    result += f"{footer}{final_stamp}"

    return result


class MdEmbedParser:
    """
    A parser for a little modification of Markdown that returns an embed.

    Reference:
        The parser will error upon finding duplicate parcel or finding more than 25 fields.
        "Sections" are areas delimited by 3 or more of a certain char. It's used to
        make things like fields, author and footer parcels.

        A normal header (`#`) makes the embed title. Any text bellow it that isn't inside
        any other parcer is considered as the description.

        Making an author or a footer parcel is done by adding a `=` section. To set the
        image for those you do `[text](url)` (the image can be ommited by leaving the
        brackets empty or removing them). To set it as an author prefix it with `#` and to
        set it as a footer use `##`.
        Timestamps can also be added in the footer section by adding a line with 
        `* t[timestamp snowflake]` (fill with the word "now" to use the current time).

        Field parcels are done with `-` sections. Inside it use a header1 (`#`) to make the
        name and the rest will be the value.




    """

    TITLE_PTRN = ...
    DESC_PTRN = ...
    AUTHOR_PTRN = ...
    FOOTER_PTRN = ...
    FIELD_PATTERN = ...

    def __init__(self):
        pass

    def __parser_load(self, text: str) -> dict:
        ...

    def __parser_dump(self, text: str) -> dict:
        ...

    def load(self, text: str) -> embeds.Embed:
        ...

    def dump(self, embed: embeds.Embed) -> str:
        ...
