#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
import importlib
import logging
from os.path import isfile

class SecretsHandler:
    def __init__(self, baker, path: str):
        self.logger = logging.getLogger("baker::secrets")
        self.baker = baker
        # format to dot notation and add the extension
        self.path = path

        self.logger.info("Doing startup load!")
        self.load()

    def load(self):
        self.logger.info(f"Attempting to open the given config file at {self.path} ...")
        if isfile(self.path):       
            self.logger.info(
                "Successfully opened the config file. Proceeding with config data loading..."
            )
        else:
            self.logger.critical(
                "Cannot open the given config file path! Make sure you passed it correctly or if it really exists!"
            )
            raise FileNotFoundError("The config file was not found")
        try:
            spec = importlib.util.spec_from_file_location("bakeryconf", self.path)
            self.data = spec.loader.load_module()
        except Exception as ex:
            self.logger.critical(
                f"Cannot properly load config data! Got the following error:"
            )
            raise ex

        self.logger.info("Successfully loaded config data!")

    def reload(self, data, full: bool = False):
        """
        Reloads the given data.
        It will reread the config file for this
        """
        self.load()

        # TODO


    def data_types(self):
        """This is bloated and I still have to work more on this"""
        attrs = {
            {sa for sa in dir(ma) if not sa.startswith("_")}
            if isinstance(ma, dict)
            else ma
            for ma in dir(self.data)
            if not ma.startswith("_")
        }
        # ### EXPANDED ###
        # attrs = set()
        # for ma in dir(self.data):
        #     if not ma.startswith("_"):
        #         if isinstance(ma, Proxy):
        #             attrs.add({sa for sa in dir(ma) if not sa.startswith("_")})
        #             continue
        #             ### EXPANDED ###
        #             for sa in dir(ma):
        #                 if not sa.startswith("_"):
        #                     attrs.add(sa)
        #             ################
        #         attrs.add(ma)
        # ################
        return attrs
