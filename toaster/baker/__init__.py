#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

__version__ = "0.0.1a"

__resources__ = {
    "local": "/bakery/toaster/resources/cache/",
    "internal": "/breadvan/",
    "global": "/breadvan/stash/",
    "resources": "/bakery/toaster/resources/",
}
