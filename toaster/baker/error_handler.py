#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-
import discord
from libneko import commands

import logging
import asyncio
import traceback
from random import choice as choice
from textwrap import dedent
from libneko import embeds

from toaster.utils import ColourConverter


ERROR_HUE = 0xFF2B2B


class ErrorHandler:
    def __init__(self, baker):
        self.logger = logging.getLogger("baker:err")
        self.baker = baker
        try:
            from toaster.resources.error_resps import RESPS

            self.resps = RESPS
        except ImportError:
            self.resps = None
            self.baker.logger.warning(
                "No error responses resource detected! Moving on with plain resps..."
            )

    async def _react_for_a_while(
        self,
        msg: discord.Message,
        emoji: str = "\N{BLACK QUESTION MARK ORNAMENT}",
        duration: int = 10,
    ) -> None:
        try:
            await msg.add_reaction(emoji)
            await asyncio.sleep(duration)
            await msg.remove_reaction(emoji, msg.guild.me)
        except (discord.Forbidden, discord.errors.NotFound):
            pass

    async def catch_error(self, ctx: commands.Context, error):
        if isinstance(error, commands.CommandNotFound):
            self.baker.bot.loop.create_task(self._react_for_a_while(ctx.message))
            return

        colour = ERROR_HUE
        if isinstance(error, UserWarning) and len(error.args) == 2:
            converter = ColourConverter()
            colour = await converter.convert(ctx, error.args[1])

        error = error.__cause__ or error
        if self.resps:
            try:
                module = error.__module__
            except AttributeError:
                module = "builtins"

            try:
                ans = self.resps[module][error.__class__.__name__]
                if isinstance(ans, list):
                    ans = choice(ans)
                caught = True

            except (KeyError, AttributeError):
                caught = False
                ans = choice(self.resps["general"])
        else:
            ans = "Whoopsey! That wasn't supposed to happen..."

        await self._respond_to_error(
            ctx, ans(error) if callable(ans) else ans, caught=caught, colour=colour
        )

        if self.baker.secrets.baker.report_errors and not caught:
            await self._file_error_report(ctx, error)

    async def _respond_to_error(
        self,
        ctx: commands.Context,
        ans: str,
        caught: bool = False,
        colour: discord.Colour = ERROR_HUE,
    ):
        if not caught:
            desc = dedent(
                f"""
                `{ans}`

                :warning: Something went wrong! This has been reported to my creator.
                Sorry for the inconvenience!
                """
            )
        else:
            desc = ans
        await ctx.send(embed=embeds.Embed(description=desc, colour=colour))

    async def _file_error_report(self, ctx: commands.Context, error):
        error = error.__cause__ or error

        self.logger.error(error, exc_info=True)

        em = embeds.Embed(
            title=":page_with_curl: Error report",
            description=dedent(
                f"""
                ```py
                {"".join(traceback.format_exception(type(error), error, error.__traceback__))}
                ```
                """
            ),
        )
        em.add_field(
            name="More info",
            value=dedent(
                f"""
                __Guild:__ {ctx.guild} ({ctx.guild.id})
                __Channel:__ {ctx.channel} ({ctx.channel.mention})
                __Author:__ {ctx.author} ({ctx.author.id})
                __Message ID:__ {ctx.message.id}
                __Message content:__ {ctx.message.content}
                """
            ),
        )
        await self.baker.bot.logs_channel.send(embed=em)
