#!/usr/bin/env python
# -*- coding: utf-8 -*-
import importlib
import inspect
import json
import logging
import os
import re
import sys
import traceback

import discord.ext.commands

logging.basicConfig(level="INFO")
logging.root.name = "get-commands.py"

with open("toaster/resources/extensions.txt") as fp:
    EXTENSIONS = tuple(
        f"{ext.split('.')[-1]}.py"
        for ext in fp.read().strip().split("\n")
        if not ext.startswith("#")
    )


def is_command(candidate):
    return isinstance(candidate, discord.ext.commands.Command)


def ref_maker():
    ref = 0
    while True:
        yield ref
        ref += 1


ref_maker = ref_maker()


def get_reference_id(command, previous_refs):
    if command is None:
        return None

    if command in previous_refs.keys():
        ref = previous_refs[command]
    else:
        ref = next(ref_maker)
        previous_refs[command] = ref

    # Cache ref to parent for completeness.
    if command.parent and command.parent not in previous_refs:
        # Recursive!
        get_reference_id(command.parent, previous_refs)

    return hex(ref)


def dump_meta(command: discord.ext.commands.Command, previous_refs):
    # noinspection PyProtectedMember
    cool_down: discord.ext.commands.Cooldown = command._buckets._cooldown
    if cool_down:
        cool_down = dict(
            type=cool_down.type.name.title(), per=cool_down.per, rate=cool_down.rate
        )
    else:
        cool_down = None

    meta = dict(
        ref=get_reference_id(command, previous_refs),
        name=command.name,
        enabled=command.enabled,
        help=command.help,
        description=command.description,
        fully_qualified_name=f"{command.full_parent_name} {command.name}".lstrip(),
        fully_qualified_aliases=[
            f"{command.full_parent_name} {a}".lstrip() for a in command.aliases
        ],
        full_parent_name=command.full_parent_name,
        examples=getattr(command, "examples", ""),
        brief=command.brief,
        usage=command.usage,
        signature=command.signature.replace(" <ctx>", ""),
        rest_is_raw=command.rest_is_raw,
        aliases=[*command.aliases],
        hidden=command.hidden,
        checks=[str(check) for check in command.checks],
        ignore_extra=command.ignore_extra,
        parent=get_reference_id(command.parent, previous_refs),
        cooldown=cool_down,
    )

    if isinstance(command, discord.ext.commands.Group):
        group: discord.ext.commands.Group = command

        meta.update(
            is_group=True,
            invoke_without_command=group.invoke_without_command,
            child_commands=[
                get_reference_id(child, previous_refs) for child in group.commands
            ],
        )
    else:
        meta["is_group"] = False

    return meta


def crawl_cog(cog, previous_refs):
    for _, obj in inspect.getmembers(cog):
        if is_command(obj):
            yield dump_meta(obj, previous_refs)


def crawl_module(single_file_module, previous_refs):
    for _, obj in inspect.getmembers(single_file_module):
        # Command in global scope
        if is_command(obj):
            yield dump_meta(obj, previous_refs)

        # Potential cog
        elif inspect.isclass(obj):
            yield from crawl_cog(obj, previous_refs)


def package_path_to_package(path):
    path = os.path.relpath(path, start=".")
    package_fragments = re.split(r"[\\/]", path)
    package = ".".join(package_fragments)
    if package.endswith(".py"):
        package = package[:-3]
    if package.endswith(".__init__"):
        package = package[:-9]
    return package


def get_candidate_module_paths_to_import_recursively(origin):
    package_path = os.path.dirname(origin)

    for dir_path, dir_names, file_names in os.walk(package_path, topdown=True):
        if dir_path.endswith("__pycache__"):
            logging.debug("Skipping __pycache__ directory %s", dir_path)

        # ignore non-python files and scripts beginning with '_' by removing from the iterator list
        for file in file_names:
            path = os.path.join(dir_path, file)

            if file == "__main__.py":
                logging.debug("Skipping __main__ to prevent bot startup...")
            elif not re.match(r".*\.py[xi]?$", file):
                logging.debug("Skipping non-python file %s", path)
            elif file not in EXTENSIONS:
                logging.debug("Non loaded extension!")
            else:
                logging.debug("Picking %s as a candidate for introspection", path)
                yield path


def crawl_module_or_package(module_or_package, previous_refs):
    origin = inspect.getfile(module_or_package)

    # If this is `__init__.py` then assume it is a package of modules
    if origin.endswith("__init__.py"):
        module_paths = get_candidate_module_paths_to_import_recursively(origin)
    else:
        module_paths = [origin]

    module_names = {
        package_path_to_package(module_path) for module_path in module_paths
    }

    logging.info("Will introspect %s files now", len(module_names))

    for module_name in module_names:
        try:
            yield from crawl_module(importlib.import_module(module_name), previous_refs)
        except (ModuleNotFoundError, ImportError):
            traceback.print_exc()
            exit(3)


def main():
    start = None
    try:
        start = sys.argv[1]
        root_module = importlib.import_module(start)
    except IndexError:
        print("Please provide a starting directory to look at the source code for.")
        exit(1)
    except (ModuleNotFoundError, ImportError) as ex:
        print(f"Failed to load {start} because: {str(ex)[0:1].lower()}{str(ex)[1:]}")
    else:
        commands = []
        previous_refs = {}

        commands += crawl_module_or_package(root_module, previous_refs)

        logging.info("DUMPING JSON TO STDOUT")

        json.dump(commands, sys.stdout, indent=3)


if __name__ == "__main__":
    main()
